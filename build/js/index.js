function openNav() {
  $('.navigation').toggleClass('active');
  $('.hamburger').toggleClass('active');
}
$('.hamburger').on( 'click',  openNav );

function popover() {
  $('.popover').toggleClass('show');
}

$('.plus').on('click', popover);

$('.popover_close').on('click', popover);
function popupOpen() {
  $('.popup').addClass('show');
  // $('body').append('<div class="fade"></div>');
}

function popupClose() {
  $('.form').addClass('show');
  $('.success').removeClass('show');
  $('.popup').removeClass('show');
  // $('.fade').remove();
}

function submitForm() {
  event.preventDefault();
  /* какой-нибудь код для отправки полей формы */
  $('.form').toggleClass('show');
  $('.success').toggleClass('show');
}

$('.success_btn').on('click', function () {
  event.preventDefault();
  $('.form').addClass('show');
  $('.success').removeClass('show');
  popupClose();
});

$('.recall_text').on('click', popupOpen);

$('.close').on('click', popupClose);

$('.form_submit').on('click', submitForm);






let controls = $('.owl-carousel');

$('.carousel .owl-carousel').owlCarousel({
  loop:true,
  items:1,
  dots:false,
  autoWidth: true,
  nav:false
});

$('.fade-slider .owl-carousel').owlCarousel({
  animateIn: 'fadeIn',
  animateOut: 'fadeOut',
  loop:true,
  items:1,
  dots:false,
  nav:false
});

$('.next').click(function() {
  controls.trigger('next.owl.carousel');
});
$('.prev').click(function() {
  controls.trigger('prev.owl.carousel');
});


//# sourceMappingURL=index.js.map
