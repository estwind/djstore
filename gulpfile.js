'use strict';

let gulp = require('gulp'),
plumber = require('gulp-plumber'),
browserSync = require('browser-sync'),
reload = browserSync.reload,
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
uglify = require('gulp-uglify'),
cssMin = require('gulp-clean-css'),
concat = require('gulp-concat'),
preFixer = require('gulp-autoprefixer'),
svgSprite = require('gulp-svg-sprite'),
svgmin = require('gulp-svgmin');

let path = {
  
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/'
  },
  src: {
    html: 'src/*.html',
    js: 'src/js/*.js',
    style: 'src/scss/*.scss',
    img: 'src/img/**/*',
    svg: 'src/svg/*',
    fonts: 'src/fonts/'
  },
  watch: {
    html: 'src/*.html',
    js: 'src/js/',
    style: 'src/scss/',
    img: 'src/img/',
    svg: 'src/svg/*'
  },
  clean: './build'
};

let config = {
  server: {
    baseDir: "./build"
  },
  tunnel: false,
  host: 'localhost',
  port: 3000,
  logPrefix: "frontend"
};

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
  .pipe(gulp.dest(path.build.html))
  .pipe(reload({stream: true}));
});

gulp.task('img:build', function () {
  return gulp.src(path.src.img)
  .pipe(gulp.dest(path.build.img));
});

gulp.task('svg:build', function () {
  return gulp.src(path.src.svg)
  .pipe(svgmin({
    js2svg: {
      pretty: true
    }
  }))
  .pipe(svgSprite({
    mode: {
      stack: {
        sprite: "../sprite.svg"
      }
    },
  }
  ))
  .pipe(gulp.dest(path.build.img));
});

gulp.task('style:build', function (done) {
  gulp.src(path.src.style)
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(sass())
  .pipe(preFixer({
    cascade: false
  }))
  .pipe(cssMin())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(path.build.css))
  .pipe(reload({stream: true}));
  done()
});

gulp.task('js:build', function () {
  return gulp.src(path.src.js)
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(concat('index.js'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(path.build.js))
  .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
  return gulp.src(path.src.fonts)
  .pipe(gulp.dest(path.build.fonts))
  .pipe(reload({stream: true}));
});

gulp.task('build', gulp.parallel('html:build', 'style:build', 'js:build', 'img:build', 'fonts:build', 'svg:build'));

gulp.task('browser-sync', function () {
  browserSync(config);
});

gulp.task('watch', function () {
  gulp.watch(path.watch.style, gulp.parallel('style:build'));
  gulp.watch(path.watch.html, gulp.parallel('html:build'));
  gulp.watch(path.watch.js, gulp.parallel('js:build'));
  gulp.watch(path.watch.img, gulp.parallel('img:build'));
  gulp.watch(path.watch.svg, gulp.parallel('svg:build'));
});

gulp.task('default', gulp.parallel('build', 'browser-sync', 'watch'));




