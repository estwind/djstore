let controls = $('.owl-carousel');

$('.carousel .owl-carousel').owlCarousel({
  loop:true,
  items:1,
  dots:false,
  autoWidth: true,
  nav:false
});

$('.fade-slider .owl-carousel').owlCarousel({
  animateIn: 'fadeIn',
  animateOut: 'fadeOut',
  loop:true,
  items:1,
  dots:false,
  nav:false
});

$('.next').click(function() {
  controls.trigger('next.owl.carousel');
});
$('.prev').click(function() {
  controls.trigger('prev.owl.carousel');
});

