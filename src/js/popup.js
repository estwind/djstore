function popupOpen() {
  $('.popup').addClass('show');
  // $('body').append('<div class="fade"></div>');
}

function popupClose() {
  $('.form').addClass('show');
  $('.success').removeClass('show');
  $('.popup').removeClass('show');
  // $('.fade').remove();
}

function submitForm() {
  event.preventDefault();
  /* какой-нибудь код для отправки полей формы */
  $('.form').toggleClass('show');
  $('.success').toggleClass('show');
}

$('.success_btn').on('click', function () {
  event.preventDefault();
  $('.form').addClass('show');
  $('.success').removeClass('show');
  popupClose();
});

$('.recall_text').on('click', popupOpen);

$('.close').on('click', popupClose);

$('.form_submit').on('click', submitForm);





